const urlCliente = "http://localhost:8080/login/"
// const urlCliente = "http://129.213.40.73:8080/retounab/login/"

const username = document.getElementById('username')
const saldo = document.getElementById('saldo')
const cuenta = document.getElementById('cuenta')
if (!sessionStorage.getItem("key")) {
    alert("acceso no autorizado")
    location.href = "index.html"
}

const mostrarCliente =async(Cliente)=>{
    username.innerHTML = Cliente.data.name
    saldo.innerHTML = new Intl.NumberFormat().format(Cliente.data.saldo)
    cuenta.innerHTML = Cliente.data.cuenta
}

const getClientes = async () => {
    try {
        const Clientes = await axios({
            method: 'GET',
            url: urlCliente + sessionStorage.getItem("user") + "/" + sessionStorage.getItem("key")
        })
        if (Clientes.status === 200) {
            mostrarCliente(Clientes);
        } 

    } catch (error) {
        setTimeout(function(){
            mensaje.style.display="none"
        }, 2000);
        mensaje.style.display="block"
    }
}

const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector))
            handler(e)
    })
}

on(document, 'click', '.btnRegresar', e => {
    location.href = "menu.html"
})
window.addEventListener('load', function () {
    getClientes()

});