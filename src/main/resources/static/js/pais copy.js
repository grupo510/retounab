const url = "http://localhost:8080/pais"


const contenedor = document.querySelector('tbody')
let resultados = ''
let accion = ''
const bcerrar = document.getElementById('cerrar')
const bcerrarx = document.getElementById('cerrarx')
const nombrePais = document.getElementById('nombre')
const modalPaises = new bootstrap.Modal(document.getElementById('modalPais'))
const formPaises = document.getElementById("frmPrincipal")
let idUsuario = ""

const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector))
            handler(e)
    })
}

on(document, 'click', '.btnCrear', e => {
    nombrePais.value = ''
    idcliente.disabled = true
    modalClientes.show()
    opcion = 'crear'
})



bcerrar.addEventListener('click', () => {
    modalPaises.hide()
})

bcerrarx.addEventListener('click', () => {
    modalPaises.hide()
})

const ajax = (options) => {
    let { url, method, success, error, data } = options;
    const xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", (e) => {
        if (xhr.readyState !== 4) return;

        if (xhr.status >= 200 && xhr.status < 300) {
            let json = JSON.parse(xhr.responseText);
            success(json);
        } else {
            let message = xhr.statusText || "Ocurrió un error";
            error(`Error ${xhr.status}: ${message}`);
        }
    });

    xhr.open(method || "GET", url);
    xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
    xhr.send(JSON.stringify(data));
};

const getUsuario = () => {

    ajax({
        url: url,
        method: "GET",
        success: (rs) => {
            console.log(rs);
            //getTiposPaises()

            setTimeout(function () {
                rs.forEach((Paises) => {

                    resultados += `<tr>
                        <td width="10%" class='btnFila'>${Paises.id}</td>
                        <td width="15%" class='btnFila'>${Paises.paisnombre}</td>
                        <td class="text-center" width="20%"><a class="btnEditar btn btn-primary">Editar</a>
                        <a class="btnBorrar btn btn-danger">Borrar</a></td>
                    </tr>`
                });
                contenedor.innerHTML = resultados
                $(document).ready(function () {
                    $('#Paises').DataTable({
                        language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },
                    });
                });
                resultados = ''
            }, 500);

        },
        error: (err) => {
            console.log(err);
            $table.insertAdjacentHTML("afterend", `<p><b>${err}</b></p>`);
        },
    });
};


const getTiposPaises = () => {

    ajax({
        url: urlTipo,
        method: "GET",
        success: (rs) => {
            console.log(rs)

            resultados = `<option value="">Seleccione Tipo de Usuario</option>`
            rs.forEach((TipoPaises) => {
                resultados += `<option value="${TipoPaises.idTipo}">${TipoPaises.descripcion}</option>
                    `
            })
            idTipoUsuario.innerHTML = resultados
            resultados = ""
        },
        error: (err) => {
            console.log(err);
            $table.insertAdjacentHTML("afterend", `<p><b>${err}</b></p>`);
        },
    });
};

const getTipoUsuario = (id) => {

    var select = document.getElementById("tipo")
    for (var i = 0; i < select.length; i++) {
        if (parseInt(select[i].value) == id)
        {
            return select[i].textContent
        }
    }
};

document.addEventListener("DOMContentLoaded", getUsuario());


document.addEventListener("click", (e) => {

    if (e.target.matches(".btnBorrar")) {

        const fila = e.target.parentNode.parentNode
        const nombre = fila.children[1].innerHTML
        const id = fila.children[0].innerHTML
        console.log(id)
        alertify.confirm('Eliminar Usuario', `¿Estás seguro de eliminar el usuario : ${nombre}?`,
            function () {
                ajax({
                    url: url + "/" + id,
                    method: "DELETE",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    success: (res) => location.reload(),
                    error: (err) => alert(err),
                });
                alertify.success('Alert Title', 'Registro eliminado')
            },
            function () {
                alertify.error('Cancel')
            });

    }
    if (e.target.matches(".btnEditar")) {
        const fila = e.target.parentNode.parentNode
        idUsuario = fila.children[0].innerHTML
        nombreUsuario.value = fila.children[1].innerHTML
        ciudadUsuario.value = fila.children[2].innerHTML
        telefonoUsuario.value = fila.children[3].innerHTML
        idTipoUsuario.value = fila.children[4].id
        accion = 'editar'
        modalPaises.show()
    }
})
formPaises.addEventListener('submit', (e) => {
    e.preventDefault()
    let metodo = "POST"
    url1 = url
    if (accion == 'editar') {
        metodo = "PUT"
        url1 = url + "/" + idUsuario
    }
    ajax({
        
        url: url1,
        method: metodo,
        headers: {
            'Content-Type': 'application/json'
        },
        success: (res) => location.reload(),
        error: (err) =>
            $form.insertAdjacentHTML("afterend", `<p><b>${err}</b></p>`),
        data: {
            "ciudad": ciudadUsuario.value,
            "nombre": nombreUsuario.value,
            "telefono": telefonoUsuario.value,
            "idTipo": idTipoUsuario.value,
        },
    });
    modalPaises.hide()
})



on(document, 'click', '.btnSalir', e => {
    location.href = "menu.html"
})