// const urlPais = "http://129.213.40.73:8080/retounab/pais/"
const urlPais = "http://localhost:8080/pais/"
const contenedor = document.querySelector('tbody')
const modalPais = new bootstrap.Modal(document.getElementById('modalPais'))
const formPais = document.querySelector('form')
const nombresPais = document.getElementById('nombres')
const idPais = document.getElementById("idPais")
let filaEditar
let resultados = ''
let opcion = ''
let idForm = 0
let tipos
let headers= {
    key : sessionStorage.getItem("key"),
    user: sessionStorage.getItem("user")
}
const mostrarPais = (Pais) => {
    tabla = $('#pais').DataTable()
    tabla.destroy()
    resultados = ""
    Pais.forEach(Pais => {
        resultados += `<tr>
                        <td  width="20%">${Pais.id}</td>
                        <td  width="60%">${Pais.paisnombre}</td>
                        <td class="text-center" width="20%">
                        <a class="btnEditar btn btn-primary">Editar</a>
                        <a class="btnBorrar btn btn-danger">Borrar</a>
                        </td>
                    </tr>`
    })
    contenedor.innerHTML = resultados
    $(document).ready(function () {
        $('#pais').DataTable({
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });
    });
}


const getPais = async () => {
    try {
        const Pais = await axios({
            method: 'GET',
            url: urlPais,
            headers: headers
        })
        return Pais.data
    } catch (error) {
       // location.href = "index.html?error=Acceso no Autorizado"
    }
}

const deletePais = async (id) => {
    try {
        const eliminar = await axios({
            method: 'DELETE',
            url: urlPais + id
        })
        alertify.success(eliminar.data)
    } catch (error) {
        console.log(error)
    }
}

function insertarFila(contenedor,id,name){
    let fila = document.createElement("tr")
    let row1 = document.createElement("td")
    let row2 = document.createElement("td")
    let row3 = document.createElement("td")
    row1.width="20%"
    row2.width="60%"
    row3.className = "text-center"
    row2.width = "20%"
    row3.innerHTML = `<a class="btnEditar btn btn-primary">Editar</a>
                    <a class="btnBorrar btn btn-danger">Borrar</a>`
    row1.textContent = id
    row2.textContent = name
    fila.appendChild(row1)
    fila.appendChild(row2)
    fila.appendChild(row3)
    contenedor.appendChild(fila)
}

const insertUpdatePais = async (id) => {
    try {
        const insertarActualizar = await axios({
            method: 'POST',
            url: urlPais,
            data: {
                id: id,
                paisnombre: nombres.value
            },
            headers: headers
        })
        if (insertarActualizar.status === 200) {

            if (opcion === "crear") {
                insertarFila(contenedor,insertarActualizar.data.id,insertarActualizar.data.paisnombre)
            } else {
                filaEditar.children[0].innerHTML = insertarActualizar.data.id
                filaEditar.children[1].innerHTML = insertarActualizar.data.paisnombre
            }

        } else if(insertarActualizar.status === 206) {
            mensaje = ''
            for (const dato of insertarActualizar.data) {
                mensaje += "Campo : " + dato.field + " Error :" + dato.message + "<br><br>";
            }
            alertify.alert('Error', mensaje);
        }
        console.log(insertarActualizar.data);
    } catch (error) {
        console.log(error)
    }
}

const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector))
            handler(e)
    })
}

on(document, 'click', '.btnBorrar', e => {
    const fila = e.target.parentNode.parentNode
    const id = fila.firstElementChild.innerHTML
    alertify.confirm("Eliminar", "Desea eliminar el Pais",
        function () {
            deletePais(id)
            fila.remove();
        },
        function () {
            alertify.error('Cancel')
        });
})

on(document, 'click', '.btnCrear', e => {
    nombresPais.value = ''
    idPais.disabled = true
    modalPais.show()
    opcion = 'crear'
})

on(document, 'click', '.btnEditar', e => {
    const fila = e.target.parentNode.parentNode
    filaEditar = fila
    idForm = fila.children[0].innerHTML
    const nombresPais = fila.children[1].innerHTML
    idPais.value = idForm
    idPais.disabled = true
    nombres.value = nombresPais
    opcion = 'editar'
    modalPais.show()
})

on(document, 'click', '.btnRegresar', e => {
    location.href = "menu.html"
})


on(document, 'click', '.enviar', e => {
    e.preventDefault()
    if (opcion == 'crear') {
        insertUpdatePais()
    }
    if (opcion == 'editar') {
        insertUpdatePais(idForm)
    }
    modalPais.hide()
})

window.addEventListener('load', async function () {
    if (!sessionStorage.getItem("key")) {
        location.href = "index.html?error=Acceso no Autorizado"
    } else {
        mostrarPais(await getPais())
    }

});