// const urlCliente = "http://129.213.40.73:8080/retounab/login"
const urlCliente = "http://localhost:8080/login/"

const username = document.getElementById('username')
const password = document.getElementById('password')
const mensaje = document.getElementById('mensaje')

const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector))
            handler(e)
    })
}

const signIn = async () => {
    try {
        const datos = await axios({
            method: 'POST',
            url: urlCliente,
            data: {
                password: password.value,
                username: username.value
            }
        })
        if (datos.status === 200) {
            console.log(datos.data)
            sessionStorage.setItem("key", datos.data.key)
            sessionStorage.setItem("user", datos.data.user)
            location.href = "menu.html"
        } else {
            mensaje.innerHTML = "Usuario o contraseña equivocados"
            setTimeout(function () {
                mensaje.style.display = "none"
            }, 2000);
            mensaje.style.display = "block"

        }
    } catch (error) {
        console.log(error)
    }
}
on(document, 'click', '.btnSignin', e => {
    signIn()
})



window.addEventListener('load', function () {
    const valores = window.location.search
    const urlParams = new URLSearchParams(valores)
    var error = urlParams.get('error');
    if (error != null) {
        mensaje.innerHTML = error
        setTimeout(function () {
            mensaje.style.display = "none"
        }, 2000);
        mensaje.style.display = "block"
    }

});