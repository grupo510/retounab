const urlCliente = "http://localhost:8080/login/"
// const urlCliente = "http://129.213.40.73:8080/retounab/login/"

if (!sessionStorage.getItem("key")) {
    alert("acceso no autorizado")
    location.href = "index.html"
}



const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector))
            handler(e)
    })
}

on(document, 'click', '.btnSalir', e => {
    sessionStorage.removeItem("key")
    sessionStorage.removeItem("user")
    location.href = "index.html"
})

on(document, 'click', '.btnClientes', e => {
    location.href = "cliente.html"
})


on(document, 'click', '.btnPaises', e => {
    location.href = "pais.html"
})
