package com.retounab.repository;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.retounab.entity.Pais;

@Repository
public interface PaisRepository extends CrudRepository<Pais,Long> {
    ArrayList<Pais>  findByPaisnombreContaining(String title);
}
