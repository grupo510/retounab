package com.retounab.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.retounab.entity.Cliente;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente,Long> {
    
    @Transactional(readOnly=true)
    @Query(value="select * from clientes WHERE username= :usuario AND password= :clave", nativeQuery = true )
    public Cliente signIn(@Param("usuario") String usuario, @Param("clave") String clave);

    Cliente findByUsernameContaining(String title);
}
