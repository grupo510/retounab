package com.retounab.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.retounab.entity.Cliente;
// import com.retounab.repository.ClienteRepository;
import com.retounab.security.Hash;
import com.retounab.service.ClienteService;

@RestController
@RequestMapping("/login")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ClienteController {
    // @Autowired
    // ClienteRepository clienteRepository;

    @Autowired
    ClienteService clienteService;

    @PostMapping()
    @ResponseBody
    public ResponseEntity<String> login(@RequestBody Cliente cliente) {
        try {
            cliente = clienteService.singIn(cliente.getUsername(), Hash.sha1(cliente.getPassword()));
            return new ResponseEntity<>(
                    "{ \"user\":\"" + cliente.getUsername() + "\",\"key\":\""
                            + Hash.sha1(cliente.getUsername() + cliente.getPassword()) + "\"}",
                    HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @CrossOrigin(origins = "*")
    @GetMapping(path = "/{user}/{key}")
    @ResponseBody
    public ResponseEntity<String> findByUsernameContaining(@PathVariable("user") String user,@PathVariable("key") String key) {
        Cliente cliente = clienteService.findByUsernameContaining(user);
        if (Hash.sha1(cliente.getUsername() + cliente.getPassword()).equals(key)) {
            return new ResponseEntity<>(clienteService.findByUsernameContaining(user).toString(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/singup")
    @ResponseBody
    public ResponseEntity<Cliente>  singUp(@Valid @RequestBody Cliente cliente) {
        Cliente obj = cliente;
        try {

            String contrasena = Hash.sha1(cliente.getPassword());
            obj.setPassword(contrasena);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST); 
        }

        return new ResponseEntity<>(clienteService.save(obj), HttpStatus.OK); 
    }

}
