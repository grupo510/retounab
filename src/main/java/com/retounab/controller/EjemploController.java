package com.retounab.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class EjemploController {
    
    
    
    @CrossOrigin(origins = "*")
    @GetMapping("/{usuario}")
    public String HelloWord(@PathVariable("usuario") String usuario) {
        return "con path parametro ";
    }
    @CrossOrigin(origins = "*")
    @GetMapping("/hello")
    public String HelloWord1(@RequestParam(required = false) String usuario, @RequestParam(required = false) String clave) {
        return "bi3n";
    }

}
