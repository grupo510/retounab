package com.retounab.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.retounab.entity.Pais;
import com.retounab.service.ClienteService;
import com.retounab.service.PaisService;

@RestController
@RequestMapping("/pais")
@CrossOrigin(origins = "*", maxAge = 3600)
public class PaisController {
    @Autowired
    PaisService PaisService;

    @Autowired
    ClienteService clienteService;

    @CrossOrigin(origins = "*")
    @GetMapping()
    public ResponseEntity<ArrayList<Pais>> getPais(@RequestHeader("key") String key,
            @RequestHeader("user") String user,@RequestParam(required = false) String dato) {
        return PaisService.findAll(key, user,dato);
    }

    @CrossOrigin(origins = "*")
    @PostMapping()
    public ResponseEntity<Pais> setPais(@Valid @RequestBody Pais pais,@RequestHeader("key") String key,
    @RequestHeader("user") String user) {
        return this.PaisService.save(pais,key,user);
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping(path = "/{idpais}")
    public ResponseEntity<String> deleteById(@PathVariable("idpais") Long idpais) {
        return this.PaisService.deleteById(idpais);
       
    }

    @GetMapping("/{id}") 
    public ResponseEntity<Pais> consultaPorId(@PathVariable Long id,@RequestHeader("key") String key,
    @RequestHeader("user") String user){ 
        return PaisService.findById(id,key,user); 
    }
}
