package com.retounab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetounabApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetounabApplication.class, args);
	}

}
