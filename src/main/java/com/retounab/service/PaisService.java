package com.retounab.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.retounab.entity.Cliente;
import com.retounab.entity.Pais;
import com.retounab.repository.PaisRepository;
import com.retounab.security.Hash;

@Service
public class PaisService {
    @Autowired
    PaisRepository PaisRepository;

    @Autowired
    ClienteService clienteService;

    public ResponseEntity<ArrayList<Pais>> findAll(String key, String user,
            @RequestParam(required = false) String title) {
        Cliente cliente = clienteService.findByUsernameContaining(user);
        if (cliente == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {

            if (Hash.sha1(cliente.getUsername() + cliente.getPassword()).equals(key)) {
                if (title == null) {
                    return new ResponseEntity<>((ArrayList<Pais>) PaisRepository.findAll(), HttpStatus.OK);
                } else {
                    return new ResponseEntity<>((ArrayList<Pais>) PaisRepository.findByPaisnombreContaining(title),
                            HttpStatus.OK);
                }
            } else {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        }
    }

    public ResponseEntity<Pais> save(Pais pais, String key, String user) {

        Cliente cliente = clienteService.findByUsernameContaining(user);
        if (cliente == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            if (Hash.sha1(cliente.getUsername() + cliente.getPassword()).equals(key)) {
                return new ResponseEntity<>(PaisRepository.save(pais), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        }
    }

    public ResponseEntity<String> deleteById(Long idPais) {
        try {
            PaisRepository.deleteById(idPais);
            return new ResponseEntity<>("El registro se borro correctamente ", HttpStatus.OK);
        } catch (Exception err) {
            return new ResponseEntity<>("Error al borrar el registro ", HttpStatus.OK);
        }
    }

    public ResponseEntity<Pais> findById(Long id, String key, String user) {
        Pais pais = new Pais();
        Cliente cliente = clienteService.findByUsernameContaining(user);
        if (cliente == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            if (Hash.sha1(cliente.getUsername() + cliente.getPassword()).equals(key)) {
                return new ResponseEntity<>(PaisRepository.findById(id).orElse(pais), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        }
    }

}
