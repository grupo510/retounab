package com.retounab.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.retounab.entity.Cliente;
import com.retounab.repository.ClienteRepository;

@Service
public class ClienteService {
    @Autowired
    ClienteRepository clienteRepository;

    public Cliente singIn(@Param("usuario") String usuario, @Param("clave") String clave){
        return  clienteRepository.signIn(usuario,clave);
    }
 
    public Cliente save(Cliente cliente) {
         return clienteRepository.save(cliente);
    }

    public Cliente findByUsernameContaining(String idcliente){
        return clienteRepository.findByUsernameContaining(idcliente);
    }
}
