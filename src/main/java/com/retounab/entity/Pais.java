package com.retounab.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "pais")
public class Pais {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
   
    private Long id;

    @NotEmpty(message = "El campo no puede estar vacio")
    @Size(min=5,max=10,message = "Debe tener entre 5 y 10 Caracteres")
    @Column(length = 10)
    private String paisnombre;
}
