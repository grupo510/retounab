package com.retounab.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "clientes")
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
   
    private Long id;

    @NotBlank
    @Column(length = 10)
    private String name;

    
    @NotBlank
    @Size(max = 15)
    @Column(unique = true)
    private String username;

    @Column(length = 45)
    private String password;

    private float saldo; 

    @Column(length = 50)
    private String cuenta;

    @Override
    public String toString() {
        return "{\"cuenta\":\"" + cuenta + "\",\"name\":\"" + name + "\", \"saldo\":" + saldo+ "}";
    } 

    
}
